#ifndef FILEGUARD_H
#define FILEGUARD_H

#include <fstream>
#include <iostream>

template <typename T>
class FileGuard : private boost::noncopyable
{
public:
    T& getFile() noexcept
    {
        return _file;
    }

    ~FileGuard()
    {
        if(_file.is_open())
        {
            _file.close();
        }
    }

private:
   T _file;
};

#endif // WRAPPERFILE_H
