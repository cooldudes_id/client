#include "client.h"

#include <string>
#include <iostream>
#include <vector>

#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

Client::Client(io_service &ios) :
    socketGuard(ios),
    ioFile(std::make_unique<IoFile>()),
    timerConnect(ios)
{
    std::cout<<__FUNCTION__<<"\n";
}

Client::~Client()
{
    std::cout<<__FUNCTION__<<"\n";
}

void Client::tryConnect()
{
    std::cout<<__FUNCTION__<<"\n";

    socketGuard.close();

    timerConnect.expires_from_now(boost::posix_time::seconds(2));
    timerConnect.async_wait([this](const boost::system::error_code&)
    {
        startConnect();
    });
}

//Waiting to connect to the server
void Client::startConnect()
{
    std::cout<<__FUNCTION__<<"\n";
    socketGuard.async_connect(ip::tcp::endpoint(ip::address::from_string("127.0.0.1"), 25000),
                               boost::bind(&Client::handleConnect, this, _1));
}

//Connection handler
void Client::handleConnect(const boost::system::error_code &err)
{
    std::cout<<__FUNCTION__<<"\n";
    if(err)
    {
        std::cout<<"Error is connect to Server: "<<err<<"\n";
        tryConnect();
        return;
    }
    std::cout<<"Connected to the Server";
    doWrite();
}

//Close socket
void Client::doClose()
{
    std::cout<<__FUNCTION__<<"\n";
    socketGuard.close();
    throw std::logic_error("timeout");
}

void Client::doWrite()
{
    std::cout<<__FUNCTION__<<"\n";

    try
    {
        std::cout<<"To exit, enter 'exit' and press 'Enter'."<<"\n";
        std::cout<<"Please, enter a file name (Example: 'filename.txt') and press 'Enter': ";

        std::string fileName;
        std::cin>>fileName;

        if(fileName == "exit")
            return;

        preparePackage(std::move(fileName));
    }
    catch(const std::exception &e)
    {
        std::cout<<"Exception: "<<e.what()<<" "<<__FUNCTION__<<"\n";
        doWrite();
    }    
}

void Client::preparePackage(std::string &&fileName)
{
    std::string package{""};
    std::cout<<"File "<<fileName<<" upload started..."<<"\n";

    auto [numBytes, lines] = ioFile->readFile(std::move(fileName));

    //
    std::string headerPackage = std::move(std::to_string(numBytes));
    size_t countDigit = std::move(headerPackage.size());
    headerPackage.insert(headerPackage.begin(), 10 - countDigit, '0');

    package.reserve(headerPackage.size()+lines.size());
    package.append(headerPackage);
    package.append(lines);

    sendPackage(std::move(package));
}

void Client::sendPackage(std::string &&package)
{
    async_write(*socketGuard.get(), buffer(package), boost::bind(&Client::handleWrite, this, _1));
}

void Client::handleWrite(const boost::system::error_code &err)
{
    std::cout<<__FUNCTION__<<"\n";
    if(err)
    {
        std::cout<<"Write error: "<<err<<"\n";
        doClose();
    }
    doReadAnswer();
}

void Client::doReadAnswer()
{
    std::cout<<__FUNCTION__<<"\n";
    //async_read(*socketGuard.get(), buffer(message, lenMessage), boost::bind(&Client::handlerReadAnswer, this, _1));
}

void Client::handlerReadAnswer(const boost::system::error_code &err)
{
    std::cout<<__FUNCTION__<<"\n";
    if(err)
    {
        std::cout<<"Error read answer: "<<err<<"\n";
        tryConnect();
        return;
    }
    std::cout<<"Message from server: "<<std::string(message);
    //std::copy(message.begin(), message.end(), std::ostream_iterator<char>(std::cout, ""));
    std::cout<<"\n";
}
