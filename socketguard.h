#ifndef SOCKETGUARD_H
#define SOCKETGUARD_H

#include <iostream>
#include <functional>

#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>

using namespace boost::asio;

class SocketGuard
{
public:
    explicit SocketGuard(io_service &ios)
        : socket(std::make_unique<ip::tcp::socket>(ios))
    {
        std::cout<<__FUNCTION__<<"\n";
    }

    ~SocketGuard()
    {
        std::cout<<__FUNCTION__<<"\n";
        close();
    }

    bool is_open() const
    {
        std::cout<<__FUNCTION__<<"\n";
        return socket->is_open();
    }

    void close()
    {
        std::cout<<__FUNCTION__<<"\n";
        if(socket->is_open())
        {
            socket->close();
        }
    }

    ip::tcp::socket* get() const noexcept
    {
        std::cout<<__FUNCTION__<<"\n";
        return socket.get();
    }

    void async_connect(const ip::tcp::endpoint &endpoint, auto &&handle)
    {
        socket->async_connect(endpoint, handle);
    }

private:
    std::unique_ptr<ip::tcp::socket> socket;
};

#endif // SOCKETGUARD_H
