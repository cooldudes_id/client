#ifndef CLIENT_H
#define CLIENT_H

#include <boost/asio.hpp>
#include <memory>

#include "iofile.h"
#include "socketguard.h"

using namespace boost::asio;

class Client : private boost::noncopyable
{
public:
    explicit Client(io_service &ios);
    ~Client();

    void tryConnect();

private:
    io_service ios;
    SocketGuard socketGuard;

    std::string headerPackage{""};
    std::string message{""};

    std::unique_ptr<IoFile> ioFile{nullptr};
    deadline_timer timerConnect;

    void preparePackage(std::string &&fileName);
    void startConnect();
    void handleConnect(const boost::system::error_code &err);
    void doClose();
    void doWrite();
    void doReadAnswer();
    void handleWrite(const boost::system::error_code &err);
    void handlerReadAnswer(const boost::system::error_code &err);
    void sendPackage(std::string &&package);
};

#endif // CLIENT_H
