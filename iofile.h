#ifndef IOFILE_H
#define IOFILE_H

#include <boost/noncopyable.hpp>
#include <string>
#include <deque>

class IoFile : private boost::noncopyable
{
public:
    std::pair<size_t, std::string> readFile(std::string &&fileName);
    void writeFile(std::string &&message);
};

#endif // IOFILE_H
