#ifndef ILOG_H
#define ILOG_H

#include <string>

class ILog
{
public:
    virtual void write(std::string &&message) = 0;
    virtual ~ILog() = default;
};

#endif // ILOG_H
