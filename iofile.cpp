#include "iofile.h"
#include "fileguard.h"

#include <iostream>
#include <fstream>
#include <memory>

std::pair<size_t, std::string> IoFile::readFile(std::string &&fileName)
{
    std::cout<<__FUNCTION__<<"\n";

    FileGuard<std::ifstream> fileGuard;
    std::ifstream &file = fileGuard.getFile();

    file.exceptions(std::ifstream::badbit);
    std::string lines{""};
    size_t numBytes{0};

    try
    {
        file.open("files/"+fileName);
        if(file.is_open())
        {
            std::string line{""};
            while (std::getline(file, line))
            {
                numBytes += line.size();
                lines.append(line);
                line.clear();
            }
        }
        else
            throw std::logic_error(std::string("I can not open the file ")+std::string("'files/")+fileName+"'\n");
    }
    catch (const std::ifstream::failure &e)
    {
        throw std::current_exception();
    }
    return std::make_pair(numBytes, lines);
}

void IoFile::writeFile(std::string &&message)
{
    std::cout<<__FUNCTION__<<" : "<<message<<"\n";

    FileGuard<std::ofstream> fileGuard;
    std::ofstream &file = fileGuard.getFile();

    file.exceptions(std::ofstream::badbit);
    try
    {
        file.open("log/log.txt", std::ios::app);
        if(file.is_open())
        {
            file << message <<"\n";
        }
        else
            std::cout << "Error open file for write"<<"\n";
    }
    catch (const std::ifstream::failure &e)
    {
        std::cerr << "Exception: "<<e.what()<<"\n";
    }
}

