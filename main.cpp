#include <iostream>
#include <thread>
#include <sstream>

#include "client.h"
#include "threadguard.h"
#include "logfile.h"

int main(int argc, char *argv[])
{   
    std::unique_ptr<ILog> log(std::make_unique<LogFile>());
    try
    {
        boost::asio::io_service ios;
        Client client(ios);
        client.tryConnect();
        ThreadGuard(std::thread([&ios]() { ios.run(); }));
        ios.stop();
    }
    catch (std::exception& e)
    {
        std::stringstream sError;
        sError<< "Exception: " << e.what() <<" - Source: "<<__FUNCTION__<< "\n";
        std::cout << sError.str();
        log->write(sError.str());
    }
    return 0;
}
