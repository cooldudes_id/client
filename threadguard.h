#ifndef THREADGUARD_H
#define THREADGUARD_H

#include <thread>

class ThreadGuard : private boost::noncopyable
{
public:
    ThreadGuard(std::thread &&thread)
        : _thread(std::move(thread))
    {
        join();
    }

    ~ThreadGuard()
    {
        std::cout<<__FUNCTION__<<"\n";
        join();
    }

private:
    std::thread _thread;

    void join()
    {
        if(_thread.joinable())
        {
            _thread.join();
        }
    }
};

#endif // THREADGUARD_H
