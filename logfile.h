#ifndef LOGFILE_H
#define LOGFILE_H

#include "ilog.h"
#include "iofile.h"

class LogFile : public ILog
{
public:
    void write(std::string &&message) override
    {
        iofile.writeFile(std::move(message));
    }

private:
    IoFile iofile;
};

#endif // LOGFILE_H
